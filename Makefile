.PHONY: test unity clean

test: unity
	make -C test

unity:
	make -C unity

clean:
	make -C test clean
	make -C unity clean


