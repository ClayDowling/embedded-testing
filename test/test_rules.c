#include "rules.h"

#include "unity_fixture.h"

TEST_GROUP(Rules);

TEST_SETUP(Rules) {}

TEST_TEAR_DOWN(Rules) {}

TEST(Rules, isalive_GivenLiveCellZeroNeighbors_ReturnsFalse) {
  TEST_ASSERT_FALSE(is_alive(true, 0));
}


TEST_GROUP_RUNNER(Rules) {
    RUN_TEST_CASE(Rules, isalive_GivenLiveCellZeroNeighbors_ReturnsFalse);
}