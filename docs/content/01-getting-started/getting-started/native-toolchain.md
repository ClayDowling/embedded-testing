---
title: "Using A Native Toolchain"
weight: 27
---

If you have a C/C++ toolchain installed on your system already you can use that for this workshop.  If you want to use this option, you should set it up before the day of the workshop, and verify that you can build a C program from the command line.  There will not be time or capacity to help you do this during the workshop.

## Windows

{{< panel status="caution" title="Visual C++" >}}

Microsoft's C compiler is a really excellent tool, but it will not work for this workshop.  The build tools used by Microsoft's compilers aren't easily compatible with the tools we'll use for embedded programming.

{{< /panel >}}

- [Chocolatey](https://chocolatey.org) is a package manager for Windows.  Installing it now will make the rest of the installation easier.
- [LLVM](http://releases.llvm.org/download.html) is a wonderful set of tools.  The compiler executable is called "clang".  `choco install llvm` in a terminal is the fastest way to install.  
- [GNU Make](https://chocolatey.org/packages/make) requires Chocolatey to install.  This is the tool we'll be using to compile all of the sources, assemble the executables, and run tests.  `choco install make` is the fastest way to install.

## OSX

OSX uses LLVM as well.  You can tell if you have it installed by running the following command from the command line:

    gcc --version

If you get a short blurb about the version of the compiler you have installed, you already have the tools you need.  Otherwise, follow the directions given in the error message.

Other packages may be useful, but we may or may not use them in the workshop.

* Homebrew, a package managere for OSX, which you'll use to install these other packages.
* pkg-config
* check (or libcheck)


## Linux

Linux systems generally use GCC for their compiler.  Test to see if you already have it installed:

    gcc --version

If you get a version number back, you're good to go.  If not, install the "build-essentials" package:

    apt install build-essentials git pkg-config check

## *BSD

BSD systems should have a C compiler installed by default.  If they do not, consult your distribution's instructions for how to install a C compiler.

You will also want:

* GNU Make (gmake), as we will be using some GNU Make specific features.
* pkg-config to properly import packages.
* check (or libcheck), an alternative testing suite.
