---
title: "GitLab WebIDE"
---

If you have a realiable network connection, the easiest way to do this workshop is via the GitLb WebIDE.

## Before You Start

1. Log in to [GitLab](https://gitlab.com).  Create an account if you don't already have one.
1. Visit the [Embedded Testing](https://gitlab.com/ClayDowling/embedded-testing) repository.
1. **FORK THIS REPOSITORY**.  You don't have commit permissions on the original repository.
1. In *your* repository, click WebIDE.

## To Make Changes

1. In the file tree on the left of the WebIDE, choose the file you want to modify.
1. Make your changes.
1. Repeat for other changes you want to make.

## Running Tests

1. Click the large "Commit" button at the bottom of the file tree.
1. Select the file(s) you want to commit (or choose the down arrow button for all files).
1. Commit the files.  After the commit is pushed the task bar at the very bottom of the screen will update with the pipeline number.
1. Right click the pipeline number at the bottom of the screen and choose "Show in new tab" to watch your build and test run.
