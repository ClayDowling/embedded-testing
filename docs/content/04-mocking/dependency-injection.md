---
title: "Dependency Injection"
date: 2019-01-01T15:34:30-05:00
weight: 50
tags:
- theory
---

Once a mock has been created, we need to inject our mock function into the code we are trying to test.  There are a couple of ways to inject a mock:

## Runtime Injection

If you are writing code which uses objects, you can often pass your mock as an argument to the constructor of the class, or via a method call which sets the object that you're mocking.  That might look like:

``` C
// blink.c
void (*blink_writer)(uint8_t pin, uint8_t state) = digitalWrite;

void blink_set_digitalWrite(void (*writer)(uint8_t, uint8_t)) {
    blink_writer = writer;
}

void blink_loop(){
    ...
    blink_writer(LED_BUILTIN, HIGH);
    ...
}

// test_blink.c
TEST(Blink, Loop_byDefault_CallsDigitalWrite) {
    blink_set_digitalWrite(mock_digitalWrite);

    blink_loop();

    TEST_ASSERT_TRUE(mock_digitalWrite_wasCalledWith(LED_BUILTIN, HIGH));
}
```

## Compile Time Injection

While runtime dependency injection is fun, the syntax is ugly.  Fortunately, because of the model that C and C++ use for assembling the final executable, it's very easy to supply our own version of `digitalWrite` in place of the version in the Arduino library.

### How C Executables Are Built

C uses a multi-step model to build the final executable.  The compile stage converts your `.c` files to machine code, stored in a `.o` file.  Following that is linking.  During linking, two things happen:

* The `.o` files are assembled into a single file.
* Function names are resolved to point to their implementation in the combined executable.
* Libraries are linked, resolving any unsatisfied function names to functions in the libraries.

### Using It

In practice, this means we can do something like this:

``` C
// mock_arduino.c
static uint8_t called_pin = 255;
static uint8_t called_state = 255;

void digitalWrite(uint8_t pin, uint8_t state) {
    called_pin = pin;
    called_state = state;
}

bool digitalWrite_wasCalledWith(uint8_t pin, uint8_t state)
{
    return pin == called_pin && state == called_state;
}

// blink.c
void blink_loop()
{
    ...
    digitalWrite(LED_BUILTIN, HIGH);
    ...
}

// test_blink.c
TEST(Blink, Loop_byDefault_CallsDigitalWrite) {

    blink_loop();

    TEST_ASSERT_TRUE(digitalWrite_wasCalledWith(LED_BUILTIN, HIGH));
}
```