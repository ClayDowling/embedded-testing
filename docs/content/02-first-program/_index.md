---
title: "Your First Build"
weight: 20
date: 2019-04-27
---

## Agenda

1. Creating a simple C program.
1. Create your first Makefile.
1. Build your program.