---
title: "Your First Program"
weight: 10
---

The very first thing we need for our project is a working build system.  To know if our build has worked, we need something to build.  We'll start with just enough to produce an executable artifact.

In the `/src` folder, create a file "main.c"

    int main(int argc, char **argv) {
        return 0;
    }

This program doesn't actually do anything except return a 0 to the calling
program.  That's enough to test that we have a working build.

