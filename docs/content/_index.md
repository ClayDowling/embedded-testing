---
title: Embedded Testing
---

# Welcome!

The Embedded Testing workshop is a way to try out Test First development for embedded software.

## Agenda

* Introduction and Workstation Setup.
* Makefiles and compiling our first program.
* Writing our first tests, with more complex makefiles.
* Mocking and dependency injection.
* Structuring code for easier testing.

Depending on available time, we will explore more complex subjects based on what the class wants.

* Other testing suites, such as [check](https://libcheck.github.io/check/) or [googletest](https://github.com/google/googletest).
* More advanced mocking.
* More advanced build tools like [CMake](https://cmake.org/).
* Closer to the metal programming with no or minimal libraries [AVR Libc](https://www.nongnu.org/avr-libc/).

### Using This Repository

Explicit permission is given to use this repository for training purposes, so long as credit is given to the original author, [Clay Dowling](http://claydowling.com)