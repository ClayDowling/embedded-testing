---
title: "Implementing the Board"
weight: 40
---

Now that you have your rules, you need a board to apply them to.  You'll recall
that in the purely mathematical game described by Conway, the board is
infinite.  For our project, however, we will be displaying the current state of
the board on a very finite [P5 Led Panel](https://amzn.to/2ZDCHib).  This
limits us to a 32 x 64 cell display.

Implement a board and apply the rules you implemented in the previous session,
in a function called `tick`.  Keep the following constraints in mind when
implementing:

1. You cannot modify the current state of the board while calculating the future state of the board.

1. What will happen when you try to apply rules to cells at the edge of the
   board?  All four corners in particular make for interesting cases.

1. Remember that C does not prevent you from indexing outside the bounds of an
   array.  Instead, it allows you to read or modify memory that doesn't belong
   to the array.  For particularly bad behavior your operating system might
   terminate your program with a segmentation fault, but when running on a
   microprocessor, it would simply allow you to corrupt your memory.  So it's
   probably a good idea to write tests that enforce good behavior at the edges
   of the board.
