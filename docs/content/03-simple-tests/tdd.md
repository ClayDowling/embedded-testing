---
title: "Test Driven Development"
weight: 10
---

Test Driven Development, sometimes called Test First Development, is a model
where you write a test which fails, then write the code to make the test pass.

* Verifies that the code does what you want.
* Ensure that code handles errors the way you want.
* Prevents breakage when you make changes later.

## Tools

In C, the easiest way to do this is to write two programs.  One is your main
program.  The other is a test program which includes all of the logic from the
main program, and tests which exercise that code.

We'll be using a test framework called
[Unity](https://github.com/throwtheswitch/unity).  It's small and easy to work
with.
