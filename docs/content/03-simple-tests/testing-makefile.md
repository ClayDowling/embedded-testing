---
title: "A Makefile for Testing"
weight: 20
---

We will write all of our tests, and the Makefile which supports them, in the
`/test` folder below the root of the workshop repository.  If you don't have
this folder now, you should create it.


Let's start with a target.

    # test/Makefile
    
    lifetest: test_rules.o test_main.o
        $(CC) $^ -o $@

That will cause the files `test_rules.c` and `test_main.c` (which don't exist
yet) to be compiled and linked into a program `lifetest`.

## Adding the Test Framework

The test framework we're using is Unity, and the source code for it is in
`/unity` along with a Makefile which will build a library, `libunity.a` from
that source.

To link our final program to the library, we must tell the compiler.

    # test/Makefile
    
    CFLAGS=-I../unity
    
    lifetest: test_rules.o test_main.o
        $(CC) $^ -o $@ -L../unity -lunity

### A New Magic Variable

Whenever a .c file is compiled to a .o file using the default rule, the value
of the `CFLAGS` variable is included as part of the compiling command.

### New Compiler Flags

`-I{path}` - Look in *path* for header files included with `#include <header.h>`

`-L{path}` - look in *path* for libraries.

`-l{name}` - use functions from the file lib*name*.a



## Adding a Unit to Test

Our file `test_rules.c` will be testing rules which are defined in
`/src/rules.c`.  To make this happen our Makefile should look like this:

    # test/Makefile
    VSRC=../src

    lifetest: test_rules.o test_main.o rules.o
        $(CC) $^ -o $@ -L../unity -lunity

### Another New Magic Variable

`VSRC` - A colon-separated list of folders to check when resolving
dependencies.  In this case, it will cause the compiler to look for rules.c in
`../src`.
