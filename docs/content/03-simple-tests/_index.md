---
title: "Simple Tests"
weight: 30
date: 2019-04-27
---

## Agenda

1. Introduce Test Driven Development.
1. A more complicated Makefile
1. Implement the rules for Conway's Game of Life.
1. Implement your board abstraction.