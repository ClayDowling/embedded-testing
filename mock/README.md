# Test Doubles

This folder contains any test doubles you need for your program.  Typically for
blink using the Arduino libraries, you will need to mock at least four
functions.  Put their implementations and their header files in this folder.

For information about writing mocks and other test doubles, consult James
Grenning's excellent [Test Driven Development for Embedded
C](https://amzn.to/2CHQMmH)
