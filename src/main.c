#include "life.h"

int main(int argc, char **argv)
{
	life_setup();

	while(1) {
		life_loop();
	}

	return 0;

}
